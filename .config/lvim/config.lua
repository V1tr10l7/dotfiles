-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny
require('v1tr10l7')

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.foldmethod = 'expr'
vim.opt.foldenable = true
vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
    {
        command = "cmake-format",
        args = { "-i" },
        filetypes = { "CMakeLists" }

    }
}
local helpers = require("null-ls.helpers")
local FORMATTING = require("null-ls.methods").internal.FORMATTING
require("null-ls").register({
    --your custom sources go here
    helpers.make_builtin({
        name = "cmake-format",
        method = FORMATTING,
        filetypes = { "CMakeLists.txt" },
        generator_opts = {
            command = "cmake-format",
            args = { "-i" }, -- put any required arguments in this table
            to_stdin = true, -- instructs the command to ingest the file from STDIN (i.e. run the currently open buffer through the linter/formatter)
        },
        factory = helpers.formatter_factory,
    })
})

lvim.format_on_save.enabled = true
lvim.plugins = {
    { "lunarvim/colorschemes" },
    { "igankevich/mesonic" },
    {
        "peterhoeg/vim-qml",
        event = "BufRead",
        ft = { "qml" },
    },
    {
        "christoomey/vim-tmux-navigator",
        lazy = false,
    },
    {
        'ilyachur/cmake4vim'
    },
    {
        "bartekprtc/gruv-vsassist.nvim"
    },
    {
        'tikhomirov/vim-glsl'
    },
    {
        'codota/tabnine-nvim'
    }
}
require("gruv-vsassist").setup({
    -- Enable transparent background
    transparent = true,

    -- Enable italic comment
    italic_comments = true,

    -- Disable nvim-tree background color
    disable_nvimtree_bg = true,

    -- Override colors (see ./lua/gruv-vsassist/colors.lua)
    color_overrides = {
        vscLineNumber = '#FFFFFF',
    },
})
lvim.colorscheme = 'gruv-vscode'
