
local local_plugins = {
    {
    { "dracula/vim" },
     {
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  config = function()
    require("nvim-tree").setup {}
  end,
},

    { "nvim-tree/nvim-tree.lua" },
    { "neovim/nvim-lspconfig" },
    { "williamboman/mason.nvim" },
    { "folke/tokyonight.nvim" },
    { "tamago324/nlsp-settings.nvim" },
    { "nvimtools/none-ls.nvim" },
    { "williamboman/mason-lspconfig.nvim" },
    { "nvim-tree/nvim-web-devicons" },
    { "sbdchd/neoformat" },
    { "lunarvim/colorschemes" },
    { "lunarvim/lunar.nvim" },
    { "Tastyep/structlog.nvim" },
    { "nvim-lua/popup.nvim" },
    { "igankevich/mesonic" },
    { "elentok/format-on-save.nvim" },
    {
        "peterhoeg/vim-qml",
        event = "BufRead",
        ft = { "qml" },
    },
    {
        "christoomey/vim-tmux-navigator",
        lazy = false,
    },
    {
        "cdelledonne/vim-cmake"
    },
        { "nvim-neotest/nvim-nio" },
        "harpoon",
        dir = "~/personal/harpoon",
        config = function()
            local harpoon = require("harpoon")

            harpoon:setup()

            vim.keymap.set("n", "<leader>a", function() harpoon:list():add() end)
            vim.keymap.set("n", "<C-e>", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

            vim.keymap.set("n", "<C-h>", function() harpoon:list():select(1) end)
            vim.keymap.set("n", "<C-t>", function() harpoon:list():select(2) end)
            vim.keymap.set("n", "<C-n>", function() harpoon:list():select(3) end)
            vim.keymap.set("n", "<C-s>", function() harpoon:list():select(4) end)
            vim.keymap.set("n", "<leader><C-h>", function() harpoon:list():replace_at(1) end)
            vim.keymap.set("n", "<leader><C-t>", function() harpoon:list():replace_at(2) end)
            vim.keymap.set("n", "<leader><C-n>", function() harpoon:list():replace_at(3) end)
            vim.keymap.set("n", "<leader><C-s>", function() harpoon:list():replace_at(4) end)
        end
    },
    {
        "vim-apm", dir = "~/personal/vim-apm",
        config = function()
            --[[
            local apm = require("vim-apm")

            apm:setup({})
            vim.keymap.set("n", "<leader>apm", function() apm:toggle_monitor() end)
            --]]
        end
    },
    {
        "vim-with-me", dir = "~/personal/vim-with-me",
        config = function() end
    },
}

return local_plugins

