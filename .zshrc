ZSH_THEME="xiong-chiamiov-plus" # set by `omz`

set -o allexport
. ./.env.zsh
set +o allexport
SAVEHIST=1000
setopt appendhistory

clear;fastfetch -l artix

CASE_SENSITIVE="true"

COMPLETION_WAITING_DOTS="false"
DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git sudo copybuffer command-not-found gh docker docker-compose python tmux zsh-autosuggestions you-should-use zsh-autocomplete vi-mode zsh-system-clipboard)
source $ZSH/oh-my-zsh.sh

[[ ! -f ~/.p10k.zsh ]] || source ~/.pk10.zsh

ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(bracketed-paste up-line-or-search down-line-or-search expand-or-complete accept-line push-line-or-edit)

source ~/.aliases.zsh

eval "$(zoxide init zsh)"
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

bindkey '\e[A' history-beginning-search-backward
bindkey '\eOA' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward
bindkey '\eOB' history-beginning-search-forward
zle -A {.,}history-incremental-search-forward
zle -A {.,}history-incremental-search-backward

zstyle ':autocomplete:*' widget-style menu-select
bindkey -M menuselect '\r' accept-line

zstyle ':autocomplete:*' list-lines 7
zstyle ':completion:*' menu select=long



eval "$(atuin init zsh)"

if [ -e /home/v1tr10l7/.nix-profile/etc/profile.d/nix.sh ]; then . /home/v1tr10l7/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
