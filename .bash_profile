#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [ -e /home/v1tr10l7/.nix-profile/etc/profile.d/nix.sh ]; then . /home/v1tr10l7/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
