alias cd="z"
alias ls="eza --icons"
alias ip="ip --color=auto"
alias ll="eza -alh --icons"
alias tree="eza --tree"
alias cat="bat -pp"
alias man="batman"
alias neofetch="fastfetch"
alias sf='fd --type f --hidden --exclude .git | fzf-tmux -p --reverse | xargs lvim'
alias sfp="fd --type f --hidden --exclude .git | fzf-tmux -p --reverse --preview='cat {}' | xargs lvim"

CMAKE_FORMAT_CONFIG_PATH="~/.config/cmake-format"
alias cmake-format="cmake-format --config-file=$CMAKE_FORMAT_CONFIG_PATH/config.py"

atuin-upgrade()
{
    bash <(curl --proto '=https' --tlsv1.2 -sSf https://setup.atuin.sh)
}
